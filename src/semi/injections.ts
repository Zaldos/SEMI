SEMIInjections = (() => {
    const doInjections = () => {
        // getSave for adding our data to a player save
        injectLoadGameRaw();

        // Event bus related injections
        injectAddItemToBank();
    };

    const injectAddItemToBank = () => {
        if (typeof addItemToBank === 'undefined') {
            console.error('addItemToBank cannot be found!');
            return;
        }

        const orgAddItemToBank = addItemToBank;
        addItemToBank = (itemID, quantity, found = true, showNotification = true, ignoreBankSpace = false) => {
            // We show the notification incase a script absorbs the item
            if (showNotification) itemNotify(itemID, quantity);

            // If a function returns true it has handled the item and we are not adding it to the bank.
            if (SEMIEventBus.AddItemToBankPre(itemID, quantity, found, showNotification, ignoreBankSpace)) {
                return true;
            }

            const result = orgAddItemToBank(itemID, quantity, found, false, ignoreBankSpace);
            if (!result) {
                return result;
            }

            SEMIEventBus.AddItemToBankPost(itemID, quantity, found, showNotification, ignoreBankSpace);

            return result;
        };
    };

    const injectLoadGameRaw = () => {
        if (typeof loadGameRaw === 'undefined') {
            console.error('loadGameRaw cannot be found!');
            return;
        }

        const orgLoadGameRaw = loadGameRaw;
        loadGameRaw = (save) => {
            let savegame = getSaveJSON(save);
            let semiKeys = 0;
            for (let storageKey in savegame) {
                if (storageKey.startsWith(`${SEMI.LOCAL_SETTINGS_PREFIX}-`) && storageKey !== savegame[storageKey]) {
                    semiKeys++;
                    localStorage.setItem(storageKey, JSON.stringify(savegame[storageKey]));
                }
            }

            console.log(`Loaded SEMI Data from Cloud Save. Found ${semiKeys} items.`);

            orgLoadGameRaw(save);
        };
    };

    function waitForLoad() {
        if (!SEMI) return;
        clearInterval(semiLoader);

        doInjections();
    }

    const semiLoader = setInterval(waitForLoad, 50);
})();
