var SEMIetcGUI = {
  thievingXP: true,
  timeRemaining: true,
  masteryEnhancements: true,
  lessNotifications: false,
  dropChances: true,
};

var { semiSetMenu } = (() => {
  const header = $('#SEMI-heading');

  const injectSidebarSections = () => {
    for (let key in SEMI.SIDEBAR_MENUS) {
      const menu = SEMI.SIDEBAR_MENUS[key];
      const menuHeader = `<li class="nav-main-heading SEMI-header" id="${SEMI.ROOT_ID}-${menu.ID}-header" ${
        menu.Title !== undefined ? `title="${menu.Title}"` : ''
      }>${menu.Header}</li>`;
      const menuSection = `<div id="${SEMI.ROOT_ID}-${menu.ID}-section-unsorted"></div>`;

      header.after(menuHeader, menuSection);
    }

    const semiNavImg = `<img class="nav-img" src="${SEMIUtils.iconSrc}">`;
    const semiNavInner = `<a class="nav-main-link nav-compact ${SEMI.ROOT_ID}-btn" id="${SEMI.ROOT_ID}-info-button">${semiNavImg}<span class="nav-main-link-name">SEMI Menu</span></a>`;
    const semiNavEl = $(`<li class="nav-main-item" id="${SEMI.ROOT_ID}-info-header">${semiNavInner}</li>`);
    $('#sidebar').find('.nav-main').append(semiNavEl);
    $(`#${SEMI.ROOT_ID}-info-button`).on('click', () => semiInfo());
  };

  const injectSEMIInfoPopup = () => {
    const semiInfoPopup = $(`
    <div class="modal" id="${
      SEMI.ROOT_ID
    }-semi-modal" tabindex="-1" role="dialog" aria-labelledby="modal-block-normal" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content"><div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark">
            <img class="nav-img" src="${SEMIUtils.iconSrc}">
            <h3 class="block-title">Scripting Engine for Melvor Idle Menu</h3>
            <div class="block-options">
              <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                <i class="fa fa-fw fa-times"></i>
              </button>
            </div>
          </div>
          <div class="block-content font-size-sm" style="padding-top: 0 !important;">
            <div style="font-size: 14pt;">Toggle SEMI features that aren't in the sidebar:</div>
            <div class="custom-control custom-switch mb-1">
              <input type="checkbox" class="custom-control-input semi-toggles" id="SEMI-auto-enable" name="SEMI-auto-enable" onchange="SEMI.setItem('remember-state', this.checked)" ${
                Boolean(SEMI.getItem('remember-state')) ? 'checked' : ''
              }>
              <label class="custom-control-label" for="SEMI-auto-enable">Automatically load previously enabled scripts</label>
            </div>
            <div class="custom-control custom-switch mb-1">
              <input type="checkbox" class="custom-control-input semi-toggles" id="SEMI-mastery-enhancements-button-enabled" name="SEMI-mastery-enhancements-button-enabled" onchange="SEMIetcGUI.masteryEnhancements = this.checked" ${
                SEMIetcGUI.masteryEnhancements ? 'checked' : ''
              }>
              <label class="custom-control-label" for="SEMI-mastery-enhancements-button-enabled">Mastery Enhancement Script: Adds progress bars for pools to skills in the menu</label>
            </div>
            <div class="custom-control custom-switch mb-1">
              <input type="checkbox" class="custom-control-input semi-toggles" id="SEMI-less-notifications-enabled" name="SEMI-less-notifications-enabled" onchange="SEMIetcGUI.lessNotifications = this.checked" ${
                SEMIetcGUI.lessNotifications ? 'checked' : ''
              }>
              <label class="custom-control-label" for="SEMI-less-notifications-enabled">Less notifications: Disables notifications for repetetive actions like auto-sell and auto-bury. Important notifications will still be shown.</label>
            </div>
            <br />
            <div style="font-size: 14pt;">
            SEMI Config Backup, Restore, and Reset:
            </div>
            <div class="block-content">
              <textarea class="form-control SEMI-static-textarea" id="exportSEMISettings" name="exportSEMISettings" rows="1" placeholder="Exported SEMI config will be here."></textarea>
              <button type="button" id="${
                SEMI.ROOT_ID
              }-semi-modal-export-button" class="btn btn-sm btn-primary" onclick="SEMI.backupSEMI()">
                Export
              </button>
            </div>
            <br>
            <div class="block-content">
              <textarea class="form-control SEMI-static-textarea" id="importSEMISettings" name="importSEMISettings" rows="1" placeholder="Paste SEMI config here."></textarea>
              <button type="button" id="${
                SEMI.ROOT_ID
              }-semi-modal-import-button" class="btn btn-sm btn-primary" onclick="SEMI.restoreSEMI()">
                Import
              </button>
            </div>
            <br>
            <br>
            <button id="hide-SEMI-info-button" class="btn btn-outline-primary" type="button">Show SEMI Info</button>
            <div id="${SEMI.ROOT_ID}-info-box" class="d-none SEMI-fixed-textbox">
              <b>New:</b> The Melvor logo in the sidebar is now wrapped in a button, clicking it navigates to the currently idling skill page.
              <br />
              <br />
              Hover over sidebar buttons or some other SEMI elements to see tooltips that describe the scripts/options and give hints.
              <br />
              <br />
              If you unlock the sidebar sections, you can drag and rearrange the items in the section. Dragging an item below the SEMI icon only visible when unlocked will hide the item when the section is locked.
              <br />
              <br />
              The SEMI GitLab repository is <a href="https://gitlab.com/aldousWatts/SEMI" target="_blank">here.</a>
              <br />
              It's where you can find the source code for SEMI, credits and developers, a wiki that describes some features, and the issues page for suggestions/bugs!
              <br />
              <br />
              Let's give a quick shout out to Christina Applegate, Malcs, and all community devs, bug reporters, and supportive helpful folks! Thanks for making Melvor & SEMI what they are today. <i class="fas fa-heart"></i>
            </div>
            <br />
            <br />
            <button id="SEMI-RESET-button" class="btn btn-danger float-right mb-4" type="button">
              <i class="fa fa-fw fa-times mr-1"></i>
              Reset SEMI
            </button>
          </div>
        </div>
      </div>
    </div>`);
    $('#modal-account-change').before(semiInfoPopup);
    $(`#hide-SEMI-info-button`).on('click', () => toggleSEMIMenuInfo());
    $(`#SEMI-RESET-button`).on('click', () => resetSEMIPrompt());
    $(`.semi-toggles`).on('click', () => setTimeout(() => saveEtcToggles(), 100));
    $(`#SEMI-auto-enable-status`).on('click', () => toggleAutoEnableScripts());
  };

  if (SEMI.getItem('etc-GUI-toggles') !== null) {
    SEMIUtils.mergeOnto(SEMIetcGUI, SEMI.getItem('etc-GUI-toggles'));
    if (SEMIetcGUI.destroyCrops !== undefined) delete SEMIetcGUI.destroyCrops;
  }

  //SEMI menu setup function
  const setupSEMI = () => {
    if ($('#auto-replant-button').length) return;
    injectSidebarSections();
    SEMIUtils.getElement('info-header').before($('<br>'));
    SEMI.setItem('etc-GUI-toggles', SEMIetcGUI); //to prevent errors first-run with thieving-calc
    SEMI.pluginNames.forEach((name) => SEMI.injectGUI(name));

    //Modal for SEMI info popup, now SEMI's menu
    injectSEMIInfoPopup();

    injectEyes();
    injectDragMenus();

    //inject current skill warp
    const logo = $('.logo-sidebar');
    logo
      .parent()
      .wrap(
        `<button class="btn btn-outline-primary p-0" onclick="SEMIUtils.changePage(SEMIUtils.currentSkillName())"></button>`
      );

    //if all goes well, yay, it's loaded
    SEMIUtils.customNotify(
      'assets/media/monsters/dragon_black.svg',
      'Scripting Engine for Melvor Idle is now loaded and running! Check the bottom of the sidebar.',
      { duration: 5000 }
    );
  };

  //show SEMI info modal function called by nav button
  const semiInfo = () => {
    SEMIUtils.getElement('semi-modal').modal(open);
  };

  const toggleSEMIMenuInfo = () => {
    $(`#${SEMI.ROOT_ID}-info-box`).toggleClass('d-none');
    if ($('#hide-SEMI-info-button').text() == 'Show SEMI Info') $('#hide-SEMI-info-button').text('Hide SEMI Info');
    else $('#hide-SEMI-info-button').text('Show SEMI Info');
  };

  const resetSEMIPrompt = () => {
    const resetResponse = prompt(
      `Wait. This will erase EVERY SINGLE SEMI CONFIGURATION SETTING. This includes every script option, every dragged menu position, every item selection on your AutoSell and such, all AutoMine preferences, EVERYTHING. This is best used for when something has gone very wrong and you'd like to reset SEMI to a fresh start.

    If you are sure you want to do this, please type 'semi' into the prompt.`,
      'I changed my mind!'
    );
    if (resetResponse === 'semi') SEMI.resetSEMI();
  };

  const saveEtcToggles = () => {
    SEMI.setItem('etc-GUI-toggles', SEMIetcGUI);
    SEMIUtils.customNotify(
      'assets/media/main/settings_header.svg',
      'Miscellaneous SEMI GUI settings saved! Changes will take place after refreshing the page.',
      { duration: 10000 }
    );
  };

  const toggleAutoEnableScripts = () => {
    SEMI.getItem('remember-state') ? SEMI.setItem('remember-state', false) : SEMI.setItem('remember-state', true);
    $(`#SEMI-auto-enable-status`).text(SEMI.getItem('remember-state') ? 'Enabled' : 'Disabled');
    $(`#SEMI-auto-enable-status`).addClass(SEMI.getItem('remember-state') ? 'btn-success' : 'btn-danger');
    $(`#SEMI-auto-enable-status`).removeClass(SEMI.getItem('remember-state') ? 'btn-danger' : 'btn-success');
  };

  const hideSemi = (reason) => {
    console.warn(`SEMI was not correctly loaded due to ${reason}`);
    SEMIUtils.getElements().toggleClass('d-none');
  };

  const loadSemi = () => {
    if (typeof SEMIUtils === 'undefined' || !SEMIUtils.utilsReady()) {
      return;
    }
    clearInterval(semiLoader);
    let tryLoad = true;
    const wrongVersion = gameVersion != SEMI.SUPPORTED_GAME_VERSION;
    if (wrongVersion) {
      const msg = `SEMI Alert:
      This version of SEMI was made for Melvor Idle ${SEMI.SUPPORTED_GAME_VERSION}. Loading the extension in this game version may cause unexpected behavior or result in errors.
      IMPORTANT NOTE: Any errors encountered after loading this way should be reported to SEMI DEVS, and NOT Malcs!
      Try loading it anyways?`;
      tryLoad = window.confirm(msg);
    }
    if (!tryLoad) {
      return hideSemi('game version incompatibility.');
    }
    // try {
    setupSEMI();
    const msg = `SEMI v${SEMI_VERSION} Loaded`;
    const suffix = wrongVersion ? ', but may experience errors.' : '!';
    toggleSEMIMenuInfo();
    console.log(msg + suffix);
    // } catch (error) {
    // hideSemi('the following error:');
    // console.error(error);
    // }
  };

  const semiLoader = setInterval(loadSemi, 200);

  return { semiSetMenu };
})();
