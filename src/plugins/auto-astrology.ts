(() => {
    const id = 'auto-astrology';
    const title = 'Auto Astrology';
    const desc =
        'Automatically study the best constellation in Astrology by this logic: If below level 99, the highest-level available. If not and the current constellation is at 99 mastery, the first without 99 mastery if such exists. Otherwise, the last constellation.';
    const imgSrc = 'assets/media/skills/astrology/astrology.svg';
    
    const astrologyOptimize = () => {
        if (SEMIUtils.currentSkillName() === 'Astrology'){
            const bestMastery = masteryCache[22].levels.findIndex(e=>e<99);
            if (skillLevel[22] < 99 && Astrology.constellations.slice().reverse().find(e=>skillLevel[22]>=e.level).id !== game.astrology.activeConstellation.id){ //max skill first
                game.astrology.studyConstellationOnClick(Astrology.constellations.slice().reverse().find(e=>skillLevel[22]>=e.level));
            } else if (masteryCache[22].levels[game.astrology.activeConstellation.id] === 99 && bestMastery!==-1){ //then max masteries
                game.astrology.studyConstellationOnClick(Astrology.constellations[bestMastery]);
            } else if (Astrology.constellations.length-1 !== game.astrology.activeConstellation.id && bestMastery===-1){ //then go back to skill xp (the final constellation)
                game.astrology.studyConstellationOnClick(Astrology.constellations.slice().reverse()[0]);
            }
        }
    };

    SEMI.add(id, {
        ms: 3000,
        pluginType: SEMI.PLUGIN_TYPE.TWEAK,
        title,
        desc,
        imgSrc,
        onLoop: astrologyOptimize,
    });
})();
