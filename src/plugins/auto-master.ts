(() => {
    const id = 'auto-master';
    const desc = `AutoMaster will automatically spend down mastery pools when they are above 95%. It will spend your mastery points on your lowest mastery item in the particular skill above 95%. Be aware that it will be affected by the Mastery XP Spending Multiplier buttons (+1, +5, +10)
    
    Additionally, this skill will claim any mastery tokens in your bank as well.`;
    const imgSrc = 'assets/media/main/mastery_pool.svg';
    const title = 'AutoMaster';

    //AutoMaster: will spend down mastery pool
    const autoMaster = () => {
        const skillList = [
            CONSTANTS.skill.Agility,
            CONSTANTS.skill.Astrology,
            CONSTANTS.skill.Cooking,
            CONSTANTS.skill.Crafting,
            CONSTANTS.skill.Farming,
            CONSTANTS.skill.Firemaking,
            CONSTANTS.skill.Fishing,
            CONSTANTS.skill.Fletching,
            CONSTANTS.skill.Herblore,
            CONSTANTS.skill.Mining,
            CONSTANTS.skill.Runecrafting,
            CONSTANTS.skill.Smithing,
            CONSTANTS.skill.Summoning,
            CONSTANTS.skill.Thieving,
            CONSTANTS.skill.Woodcutting,
        ];

        const masteryTokens = [
            CONSTANTS.item.Mastery_Token_Agility,
            CONSTANTS.item.Mastery_Token_Astrology,
            CONSTANTS.item.Mastery_Token_Cooking,
            CONSTANTS.item.Mastery_Token_Crafting,
            CONSTANTS.item.Mastery_Token_Farming,
            CONSTANTS.item.Mastery_Token_Firemaking,
            CONSTANTS.item.Mastery_Token_Fishing,
            CONSTANTS.item.Mastery_Token_Fletching,
            CONSTANTS.item.Mastery_Token_Herblore,
            CONSTANTS.item.Mastery_Token_Mining,
            CONSTANTS.item.Mastery_Token_Runecrafting,
            CONSTANTS.item.Mastery_Token_Smithing,
            CONSTANTS.item.Mastery_Token_Summoning,
            CONSTANTS.item.Mastery_Token_Thieving,
            CONSTANTS.item.Mastery_Token_Woodcutting,
        ];

        masteryTokens.forEach((token) => {
            const tokenSkill = items[token].skill;
            const poolSize = getMasteryPoolTotalXP(tokenSkill);
            const currPool = MASTERY[tokenSkill].pool;
            const poolRatio = currPool / poolSize;

            if (SEMIUtils.getBankQty(token) && poolRatio < 1) {
                selectBankItem(token);
                claimToken();
            }
        });

        for (const skillId of skillList) {
            const poolSize = getMasteryPoolTotalXP(skillId);
            const currPool = MASTERY[skillId].pool;

            if (currPool / poolSize > 0.95) {
                // Creates an array with the remaining mastery xp to next level
                const skillMasteryXpToNextLevel = MASTERY[skillId].xp.map((skill, index) =>
                    getMasteryXpForNextLevel(skillId, index)
                );

                const poolIsMax = currPool / poolSize === 1;
                // Filters out the 0 xp remaining max levels and then finds the minimum xp remaining
                const skillsNoZeros = skillMasteryXpToNextLevel.filter((xp) => xp > 0);

                // If the pool is maxed out and all skills are maxed out, continue
                if (poolIsMax && skillsNoZeros.length === 0) {
                    continue;
                }

                const skillMinXpRemaining = Math.min(...skillsNoZeros);

                const skillIdtoLevel = skillMasteryXpToNextLevel.indexOf(skillMinXpRemaining);

                const remainingPostSpend = (currPool - skillMinXpRemaining) / poolSize;

                if (remainingPostSpend > 0.95) {
                    levelUpMasteryWithPool(skillId, skillIdtoLevel);
                    //auto-close the modal, just spend it
                    $('#modal-spend-mastery-xp').modal('hide');
                } else if (poolIsMax) {
                    const minSkill = MASTERY[skillId].xp.indexOf(Math.min(...MASTERY[skillId].xp));
                    levelUpMasteryWithPool(skillId, minSkill);
                    $('#modal-spend-mastery-xp').modal('hide');
                }
            }
        }
    };

    SEMI.add(id, { ms: 2000, onLoop: autoMaster, title, desc, imgSrc });
})();
