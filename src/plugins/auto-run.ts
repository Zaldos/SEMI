(() => {
    const id = 'auto-run';
    const title = 'AutoRun';
    const desc =
        'AutoRun will automatically run from combat if you are out of food, runes, or ammo, or if you can be one-hit-killed by the enemy.';
    const imgSrc = 'assets/media/skills/combat/run.svg';

    const autoRun = () => {
        if (!SEMIUtils.isCurrentSkill('Hitpoints')) {
            return;
        }
        const usingRanged = player.attackType === 'ranged';
        const ammo = player.equipment.slots.Quiver.quantity;
        const sling = player.equipment.slots.Quiver.item.id === CONSTANTS.item.Slingshot;

        const usingMagic = player.attackType === 'magic';

        const usingMelee = player.attackType === 'melee';

        const hpmax = SEMIUtils.maxHP();
        const deadlyEnemyMaxHit = SEMIUtils.adjustedMaxHit() > hpmax;
        if (deadlyEnemyMaxHit) {
            return runFromCombat(
                `courage: the adjusted max hit of the current enemy (${SEMIUtils.maxHitOfCurrentEnemy()} raw DMG, ${SEMIUtils.adjustedMaxHit()} total DMG) is greater than your max hp! (${hpmax} HP)`
            );
        }
        if (player.food.slots[player.food.selectedSlot].quantity < 1) {
            return runFromCombat('food.');
        }

        // If we have no ammo, run
        if (usingRanged && ammo < 1 && !sling) {
            return runFromCombat('ammo.');
        }

        // This complicated string first checks which style of spell you are using, then uses the checkForItems function to ensure that the runes exist in the bank
        if (
            usingMagic &&
            ((!player.usingAncient &&
                !player.manager.bank.checkForItems(player.getRuneCosts(SPELLS[player.spellSelection.standard]))) ||
                (player.usingAncient &&
                    !player.manager.bank.checkForItems(player.getRuneCosts(ANCIENT[player.spellSelection.standard]))))
        ) {
            return runFromCombat('runes.');
        }

        // if we have no weapon, run
        if (!SEMIUtils.currentEquipmentInSlot('Weapon')) {
            return runFromCombat('weapon.');
        }
    };

    /** @param {string} reason */
    const runFromCombat = (reason) => {
        const today = new Date();
        const date = today.toDateString();
        const time = today.toTimeString().split(' ')[0];
        const dateTime = `${date} @ ${time}`;
        SEMIUtils.stopSkill('Hitpoints');
        SEMIUtils.customNotify(
            imgSrc,
            `SEMI: Exited Auto Combat @ ${dateTime} because ${username} is out of ${reason}`
        ); //upgrade to jqueryui modal dialog
        console.log(`SEMI: Exited Auto Combat @ ${dateTime} because ${username} is out of ${reason}`);
        if (SEMI.isEnabled('auto-slayer')) {
            SEMI.disable('auto-slayer');
        }
    };

    SEMI.add(id, { ms: 500, onLoop: autoRun, pluginType: SEMI.PLUGIN_TYPE.AUTO_COMBAT, title, desc, imgSrc });
})();
