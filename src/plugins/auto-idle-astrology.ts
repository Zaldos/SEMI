(() => {
    const id = 'auto-idle-astrology';
    const title = 'Auto Idle Astrology';
    const desc =
        'This will automatically begin astrology whenever your character is idle. The highest-level constellation available will be used.';
    const imgSrc = 'assets/media/bank/skillcape_astrology.png';
    
    const astrologyIfIdle = () => {
        if (SEMIUtils.currentSkillName() === '' && !game.isUnpausing) { //idle
            game.astrology.studyConstellationOnClick(Astrology.constellations.slice().reverse().find(e=>skillLevel[22]>=e.level));
        }
    };

    SEMI.add(id, {
        ms: 3000,
        pluginType: SEMI.PLUGIN_TYPE.TWEAK,
        title,
        desc,
        imgSrc,
        onLoop: astrologyIfIdle,
    });
})();
